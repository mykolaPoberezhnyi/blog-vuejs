import Vue from 'vue'
// import Vuex from 'vuex'
import axios from 'axios'
import router from 'vue-router'
import ElementUI from 'element-ui'
import App from './App.vue'

Vue.use(router);
Vue.use(axios); 
Vue.use(ElementUI);

new Vue({
  el: '#app',
  render: h => h(App)
})